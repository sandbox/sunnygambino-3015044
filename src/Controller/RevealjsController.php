<?php
namespace Drupal\revealjs\Controller;
use Drupal\Core\Controller\ControllerBase;
/**
 * Defines RevealjsController class.
 */
class RevealjsController extends ControllerBase {
  /**
   * Returns a page title.
   */
  public function getTitle() {
    return  $this->t('Revealjs');
  }
  /**
   * Display the markup.
   *
   * @return array
   *   Return markup array.
   */
  public function content() {
    $form = \Drupal::formBuilder()->getForm('Drupal\revealjs\Form\RevealjsForm');
    $form['#markup'] = $this->t('Hello, Revealjs');
    return $form;
  }
}
